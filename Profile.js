import React from 'react'
import {TouchableOpacity, Text} from 'react-native'
import {Actions} from 'react-native-router-flux'

const Profile = ()=>{
	const goToHome = ()=>{
		Actions.profile()
	}
	return(
		<TouchableOpacity onPress={goToHome}>
			<Text>
				Mi perfil
			</Text>
		</TouchableOpacity>
		)
}

export default Profile;
