import React from 'react'
import {TouchableOpacity, Text} from 'react-native'
import {Actions} from 'react-native-router-flux'

const Home = ()=>{
	const goToProfile = ()=>{
		Actions.profile()
	}
	return(
		<TouchableOpacity onPress={goToProfile}>
			<Text>
				Mi perfil
			</Text>
		</TouchableOpacity>
		)
}

export default Home;
